
import random
from collections import defaultdict
import csv
import numpy as np
from tqdm import tqdm

total_players = 100_000
total_games = 10_000

brackets = {
    "C": ["C-", "C", "C+"],
    "B": ["B-", "B", "B+"],
    "A": ["A-", "A", "A+"],
    "S": ["S"],
    "S+": ["S+"],
}

bracket_sequence = list(brackets.keys())

subbracket_breakpoints = {
    "C": [0, 200, 400],
    "B": [0, 350, 600],
    "A": [0, 500, 800],
    "S": [0],
    "S+": [0],
}

bracket_starting_points = {
    "C": 0,
    "B": 100,
    "A": 200,
    "S": 300,
    "S+": 300,
}

rank_up_challenge_breakpoint = {
    "C": 600,
    "B": 850,
    "A": 1100, # no this isnt a typo its actually like that
    "S": 1000,
}

series_win_points =  {
    "C": {0: 0, 1: 20, 2: 45, 3: 75, 4: 110, 5: 150},
    "B": {0: 0, 1: 30, 2: 65, 3: 105, 4: 150, 5: 200},
    "A": {0: 0, 1: 40, 2: 85, 3: 135, 4: 190, 5: 250},
    "S": {0: 0, 1: 50, 2: 105, 3: 165, 4: 230, 5: 300},
    "S+": {0: 0, 1: 50, 2: 105, 3: 165, 4: 230, 5: 300}
}

series_cost = {
    'C-': 0,
    'C': 20,
    'C+': 40,
    'B-': 55,
    'B':  70,
    'B+': 85,
    'A-': 110,
    'A': 120,
    'A+': 130,
    'S': 170,
    'S+': 180,
}

enable_rank_down = False

rank_down_starting_points = {
    "C": 200,
    "B": 350,
    "A": 500,
    "S": 300,
}

enable_medals = True

# bunch of unfinished features...
enable_player_skill = False
enable_player_dedication = False

def simulate_medals(skill):
    points_distribution = [0, 1, 5]
    
    if not enable_player_skill:
        return sum(random.choices(points_distribution))

    weights = [
        max(0, 1 - skill * 1.5),  # Probability for 0 point decreases faster with skill
        max(0, min(1, 0.3 + 0.5 * (1 - abs(skill- 0.5) * 2))),  # 1 point probability is high around skill level 0.5
        max(0, min(1, skill * 1.2))  # 5 point probability increases with skill
    ]
    
    medals = random.choices(points_distribution, weights=weights, k=3)

    total_points = sum(medals)
    
    return total_points

class Player:
    def __init__(self, skill = 1.0, dedication = 1.0):
        self.bracket = 'C'
        self.sub_bracket = 'C-'
        self.points = 0
        
        self.skill = 1.0
        self.dedication = 1.0

        if enable_player_skill:
            self.skill = skill
        if enable_player_dedication:
            self.dedication = dedication

        self.doing_rank_up = False
        self.series_wins = 0
        self.series_losses = 0
        self.series_medal_points = 0

        self.total_games = 0

        self.games_to_reach_bracket = {
            "C": 0,
            "B": None,
            "A": None,
            "S": None,
            "S+": None,
        }

    def play_game(self):
        self.total_games += 1
        # win/loss is 50/50
        if random.random() > 0.5:
            self.series_wins += 1
        else:
            self.series_losses += 1

        # random badge points awarded
        if enable_medals:
            self.series_medal_points += simulate_medals(self.skill)

        # figure out if you should reset or not
        series_reset = False

        # Check if won/lost rank up challenge
        if self.doing_rank_up and self.series_wins >= 3:
            self.raise_bracket()
            self.reset_series()
            if self.games_to_reach_bracket[self.bracket] is None:
                self.games_to_reach_bracket[self.bracket] = self.total_games
            self.doing_rank_up = False
            series_reset = True
        elif self.doing_rank_up and self.series_losses >= 3:
            self.reset_series()
            self.doing_rank_up = False
            series_reset = True
        
        # Reward points at end of normal series
        if self.series_wins >= 5 or (self.series_losses >= 3 and not self.doing_rank_up):
            self.points += \
                series_win_points[self.bracket][self.series_wins] + self.series_medal_points
            self.update_sub_bracket()
            self.reset_series()
            series_reset = True

        # Start next series
        if self.points >= 1000 and series_reset and self.bracket != "S+":
            self.doing_rank_up = True
            self.points -= series_cost[self.sub_bracket]
        elif series_reset:
            self.points -= series_cost[self.sub_bracket]

    def reset_series(self):
        self.doing_rank_up = False
        self.series_wins = 0
        self.series_losses = 0
        self.series_medal_points = 0

    def raise_bracket(self):
        self.bracket = \
            bracket_sequence[min(len(brackets), bracket_sequence.index(self.bracket) + 1)]
        self.sub_bracket = brackets[self.bracket][0]
        self.points = bracket_starting_points[self.bracket]

    def update_sub_bracket(self):
        breakpoints = subbracket_breakpoints[self.bracket]
        bracket_index = brackets[self.bracket].index(self.sub_bracket)
        # naive rank down system
        if enable_rank_down:
            # Demote subbracket if points less than breakpoint
            if breakpoints[bracket_index] <= self.points:
                self.sub_bracket = breakpoints[max(bracket_index - 1, 0)]
            # Demote bracket if points negative
            if self.points < 0 and self.bracket != "C":
                self.bracket = bracket_sequence[bracket_sequence.index(self.bracket) - 1]
                self.sub_bracket = self.bracket
                self.points = rank_down_starting_points[self.bracket]

        for i in range(0, len(breakpoints)):
            if self.points >= breakpoints[i] and i >= bracket_index:
                self.sub_bracket = brackets[self.bracket][i]


print(f"""\
{total_players=}
{total_games=}
{enable_rank_down=}
{enable_medals=}
{enable_player_skill=}
{enable_player_dedication=}
""")

# Simulate the system
# skill = np.random.uniform(low=0.0, high=1.0, size=total_players)
dedication = np.random.uniform(low=0.0, high=1.0, size=total_players)
players = [Player(skill=0.8, dedication=dedication[i]) for i in range(0, total_players)]

for i in tqdm(range(0, total_games)):
    for player in players:
        if enable_player_dedication and player.dedication < i/total_games:
            continue
        player.play_game()

# Crunch the numbers...
subbracket_count = defaultdict(lambda: {"count": 0, "points": [], "percentage": 0.0})
bracket_count = defaultdict(lambda: {"count": 0, "points": [], "percentage": 0.0})

for player in players:
    subbracket_count[player.sub_bracket]["count"] += 1

for bracket in subbracket_count.keys():
    subbracket_count[bracket]["percentage"] = \
        round(subbracket_count[bracket]["count"] / len(players) * 100, 2)

for player in players:
    bracket_count[player.bracket]["count"] += 1

for bracket in bracket_count.keys():
    bracket_count[bracket]["percentage"] = \
        round(bracket_count[bracket]["count"] / len(players) * 100, 2)
print()
print("Rank\tPlayers\tPercentage")
print("-----")
for i in series_cost.keys():
    print(f"{i}:\t{subbracket_count[i]['count']}\t({subbracket_count[i]['percentage']})")

print("-----")
for i in bracket_sequence:
    print(f"{i}:\t{bracket_count[i]['count']}\t({bracket_count[i]['percentage']})")

name = input("name data? ")

with open(f'{name}.csv', "w") as f:
    writer = csv.writer(f)
    writer.writerow(
        ["bracket", "sub bracket", "points", "skill", "dedication", *[f"games to reach {b}" for b in bracket_sequence]])
    for player in players:
        writer.writerow(
            [player.bracket, 
             player.sub_bracket, 
             player.points, 
             player.skill, 
             player.dedication, 
             *[value for value in player.games_to_reach_bracket.values()]])

print(f"Data written to {name}.csv in current directory")

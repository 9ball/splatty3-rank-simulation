import csv
import matplotlib.pyplot as plt

the_data = []

name = input("name of dataset? ")

with open(f'{name}.csv', mode='r') as f:
    reader = iter(csv.reader(f))
    
    # skip the first element
    next(reader)

    for rows in reader:
        the_data.append({
            "bracket": rows[0],
            "sub bracket":rows[1],
            "points":int(rows[2]),
            "skill":float(rows[3]),
            "dedication":float(rows[4]),
            "games to reach C":rows[5] or None,
            "games to reach B":rows[6] or None,
            "games to reach A":rows[7] or None,
            "games to reach S":rows[8] or None,
            "games to reach S+":rows[9] or None,
        })

ranks = ["C-", "C", "C+", "B-", "B", "B+", "A-", "A", "A+", "S", "S+"]
brackets = ["C", "B", "A", "S", "S+"]
total = len(the_data)

players_per_rank = [(rank, sum([p["sub bracket"] == rank for p in the_data])) for rank in ranks]
players_per_rank_perc = [(rank, count/total*100) for rank,count in players_per_rank]
players_per_bracket = [(rank, sum([p["bracket"] == rank for p in the_data])) for rank in brackets]
players_per_bracket_perc = [(rank, count/total*100) for rank,count in players_per_bracket]

debtors_per_rank = [(rank, sum([p["sub bracket"] == rank and p["points"] < 0 for p in the_data])) for rank in ranks]
debtors_per_rank_perc = [(rank, count/total*100) for rank,count in debtors_per_rank]
debtors_per_bracket = [(rank, sum([p["bracket"] == rank and p["points"] < 0 for p in the_data])) for rank in brackets]
debtors_per_bracket_perc = [(rank, count/total*100) for rank,count in debtors_per_bracket]

print(players_per_rank_perc)
print(players_per_bracket_perc)
print(debtors_per_rank_perc)
print(debtors_per_bracket_perc)

counts = [i[1] for i in players_per_rank_perc]
plt.gca().set_ylim([0, 50])
plt.bar(ranks, counts)
plt.title(name + " - Player Population")
plt.xlabel("Rank")
plt.ylabel("% of players")
plt.savefig(name + " - Player Population")
counts = [i[1] for i in debtors_per_rank_perc]
plt.gca().set_ylim([0, 100])
plt.bar(ranks, counts)
plt.title(name + " - Debtors")
plt.title(name + " - Player Population")
plt.xlabel("Rank")
plt.ylabel("% of players")
plt.savefig(name + " - Debtors")
